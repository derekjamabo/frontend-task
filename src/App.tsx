import React from 'react';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.scss';
import Dashboard from './components/Dashboard';

import Results from "./components/Results";
import Finalize from "./components/Finalize";
// import useTestContext, {TestContext} from "./services/Api";



function App() {
    // const  textContext = useTestContext();
  return (
      // <TestContext.Provider value={textContext}>
      <Router>
          <div className="App">
              <nav className="navBar">
                  <div className="title">
                      Dashboard
                  </div>
              </nav>
              <Switch>
                  <Route exact path="/" component={Dashboard}/>
                  <Route  path='/results/:testId' component={Results}/>
                  <Route  path='/finalize/:testId' component={Finalize}/>
              </Switch>
          </div>
      </Router>
      // </TestContext.Provider>
  );
}

export default App;
