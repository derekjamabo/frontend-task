import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";


export enum Type {
    CLASSIC = "CLASSIC",
    SERVER_SIDE = "SERVER_SIDE",
    MVT = "MVT"
}

export enum Status {
    DRAFT = "DRAFT",
    ONLINE = "ONLINE",
    PAUSED = "PAUSED",
    STOPPED = "STOPPED",
}

export interface Site {
    id: number;
    url: string;
}

export interface Test {
    id: number;
    name: string;
    type: Type;
    status: Status;
    siteId: number;
    url?: string;
    btnLink?: string;
}

export default function  TestDetail(testId: string){
    const [testDetail, setDetail] = useState<Test>();

    useEffect(() => {
        fetch('http://localhost:3100/tests/'+ testId ).then(res=> res.json()).then(result => {
            setDetail(result);
        })
    },[testId])

    return (
        <div className='page'>
            <p>
                {
                    testDetail?.name
                }
            </p>
            <Link to='/'  className={'back-btn'}>
                Back
            </Link>

        </div>
    );
}

