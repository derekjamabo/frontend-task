import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {Site, Test, Type} from "../Shared";

// Enum for tracking sort state
enum ORDER {
    ASC = "ASC",
    DESC = "DESC"
}


/* Dashboard page*/
const Dashboard: React.FC = () => {

    const [tests, setTests] = useState<Test[]>([]); // initail test data
    const [search, setSearch] = useState(''); // search state
    const [filter, setFilter] = useState<Test[]>([]); // filter state...
    const [sortOrder, setOrder] = useState(''); // sort state


    /*
    * Retrieve test data from the api
    * */
    useEffect(() => {
        // first get the test from the test endpoint
        fetch('http://localhost:3100/tests').then((response => response.json()))
            .then((testResults: Test[]) => {

                //get the sites from the sites end point
                fetch('http://localhost:3100/sites')
                    .then((siteResponse) => siteResponse.json())
                    .then((siteResults: Site[]) => {

                        /*
                        *  Merge results to create the appropriate data set
                        *
                        * */
                        let newSiteResults: Test[] = [];
                        if (testResults.length > 0 && siteResults.length > 0) {
                            testResults.map(el => {
                                siteResults.forEach(res => {
                                    if (el.siteId === res.id) {
                                        const url = res.url as string;
                                        // remove url protocol (https, http and www)
                                        el.url = url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "");
                                    }
                                    //  Assign route links to item  4 Results. 3 Finalise
                                    switch (el.type) {
                                        case Type.CLASSIC:
                                            el.btnLink = 'results'
                                            break;
                                        case Type.MVT:
                                            el.btnLink = el.status !== 'STOPPED' ? 'finalize' : 'results';
                                            break;
                                        case Type.SERVER_SIDE:
                                            el.btnLink = el.status !== 'DRAFT' ? 'results' : 'finalize';
                                            break;
                                        default:
                                            el.btnLink = 'finalize'
                                    }
                                })
                                return el;
                            })
                            newSiteResults = testResults;
                        }
                        setTests(newSiteResults)
                    })
            });
    }, []);


    /*
    * initiate filter/search
    * */
    useEffect(() => {
        setFilter(
            tests.filter(item => JSON.stringify(item).toLowerCase().includes(search.toLowerCase())
            )
        )
    }, [search, tests])


    // sort function
    const handleSort = ((type: string) => {

        //condition for status sort
        if (type === 'status') {
            const order = ['draft', 'stopped', 'paused', 'online'];

            // desc sort
            if (sortOrder !== ORDER.ASC) {
                tests.sort((a, b) => {
                    return order.indexOf(a.status.toLowerCase()) - order.indexOf(b.status.toLowerCase());
                })
                setOrder(ORDER.ASC);

            } else {
                // asc sort for status
                order.reverse();
                tests.sort((a, b) => {
                    return order.indexOf(a.status.toLowerCase()) - order.indexOf(b.status.toLowerCase());
                })
                setOrder(ORDER.DESC);
            }
        } else {

            // every other title sort
            if (sortOrder !== ORDER.ASC) {
                tests.sort((a, b) => {
                    // @ts-ignore
                    return a[type].localeCompare(b[type]);
                });
                setOrder(ORDER.ASC)
            } else {
                tests.sort((a, b) => {
                    // @ts-ignore
                    return b[type].localeCompare(a[type]);
                });
                setOrder(ORDER.DESC)
            }
        }
        setFilter(tests);
    })
    return (
        <div className={'page'}>
            <div className='search'>
                <input type="search" placeholder={'What test are you looking for?'} className={'searchInput'}
                       onChange={(e) => setSearch(e.target.value)}/>
                <span className={'tests-count'}>{filter.length} tests</span>
            </div>
            <div className="data-table">
                <table>
                    <thead>
                    <tr>
                        <th onClick={() => handleSort('name')}>Name</th>
                        <th onClick={() => handleSort('type')}>Type</th>
                        <th onClick={() => handleSort('status')}>Status</th>
                        <th onClick={() => handleSort('url')}>Site</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        filter.map((item: Test, idx) => (
                            <TestItems key={idx} {...item}/>
                        ))
                    }
                    </tbody>

                </table>
            </div>
        </div>
    )
}

const TestItems = (props: Test) => {
    const btnClass = 'btn ' + props.btnLink?.toLowerCase();

    return (

        <>
            <tr key={props.id.toString()}>
                <td>
                    {
                        props.name
                    }
                </td>
                <td className={'item-type text-capitalize'}>
                    {
                        props.type
                    }
                </td>
                <td className={props.status.toLowerCase()}>
                    {
                        props.status
                    }
                </td>
                <td>
                    {
                        props.url
                    }
                </td>
                <td>
                    <Link to={`${props.btnLink}/${props.id} `} className={btnClass}>
                        {props.btnLink}
                    </Link>
                </td>
            </tr>
        </>
    )

}
export default Dashboard;
