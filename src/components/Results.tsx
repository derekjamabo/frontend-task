import React from "react";
import { useParams} from "react-router-dom";
import TestDetail from "../Shared";

 const Results: React.FC = () => {
     // @ts-ignore
     let { testId } = useParams();
     const testDetail = TestDetail(testId);
     return (
         <>
             {
                 testDetail
             }
         </>
     );

}
export  default Results;
